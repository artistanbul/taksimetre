﻿
function tabbs() {
    document.getElementById('map_canvas').style.display = 'none';
        if (navigator.geolocation) {
              //   document.getElementById('status').innerHTML = 'Checking...';
             
            
              navigator.geolocation.getCurrentPosition(
      onSuccess,
      onError, {
          enableHighAccuracy: true,
          timeout: 20000,
          maximumAge: 120000
      });
  }
  else {
      // document.getElementById('status').innerHTML = 'Geolocation not supported.';
  }
          resizeGame();
          // get tab container
          var container = document.getElementById("tabContainer");
          // set current tab
          var navitem = container.querySelector(".tabs ul li");
          //store which tab we are on
          var ident = navitem.id.split("_")[1];
          navitem.parentNode.setAttribute("data-current", ident);
          //set current tab with class of activetabheader
          navitem.setAttribute("class", "tabActiveHeader");

          //hide two tab contents we don't need
          var pages = container.querySelectorAll(".tabpage");
          for (var i = 1; i < pages.length; i++) {
              pages[i].style.display = "none";
          }

          //this adds click event to tabs
          var tabs = container.querySelectorAll(".tabs ul li");
          for (var i = 0; i < tabs.length; i++) {
              tabs[i].onclick = displayPage;
          }
      }

      // on click of one of tabs
      function displayPage() {
          var current = this.parentNode.getAttribute("data-current");
          //remove class of activetabheader and hide old contents
          document.getElementById("tabHeader_" + current).removeAttribute("class");
          document.getElementById("tabpage_" + current).style.display = "none";

          var ident = this.id.split("_")[1];
          //add class of activetabheader to new active tab and show contents
          this.setAttribute("class", "tabActiveHeader");
          document.getElementById("tabpage_" + ident).style.display = "block";
          this.parentNode.setAttribute("data-current", ident);
         // initialize();
      }
      var directionsService = new google.maps.DirectionsService();
      var directionsDisplay = new google.maps.DirectionsRenderer();
      var map;

      var firstBoolindex = 0;
      var secondBoolindex = 0;
      var thirdBoolindex = 0;
      function mainControl1() {

               firstBoolindex = 1;
              secondBoolindex = 0;
              thirdBoolindex = 0;
              document.getElementById('button_div').innerHTML = ' ';
             // document.getElementById('map_canvas'­ ).style.visibility = "hidden"
              document.getElementById('map_canvas').style.display = 'none';
              resetMarks();
          
      }

      function mainControl3() {

          firstBoolindex = 0;
          secondBoolindex = 0;
          thirdBoolindex = 1;
          document.getElementById('button_div').innerHTML = ' ';
          // document.getElementById('map_canvas'­ ).style.visibility = "hidden"
          document.getElementById('map_canvas').style.display = '';
          resetMarks();
          
      }
       var infowindow2;

       function getTaxi() {
           var pyrmont = new google.maps.LatLng(positionLat, positionLong);
        var request = {
          location: pyrmont,
          radius: 50000,
          types: ['bar']
      };
     
        infowindow2 = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.search(request, callback);
        
        }

      function callback(results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker2(results[i]);
          }
        }
      }

      function createMarker2(place) {
         
          var placeLoc = place.geometry.location;
          marker = new google.maps.Marker({
              map: map,
              position: place.geometry.location
              
          });
      }
      function mainControl2() {

          document.getElementById('map_canvas').style.display = '';
         document.getElementById('button_div').innerHTML='<input type="button" id="button" onclick="deneme()" data-localize="calculate" value="Calculate"/><input type="button" onclick="resetMarks()" id="button"  value="Reset" />'
              firstBoolindex = 0;
              secondBoolindex = 1;
              thirdBoolindex = 0;
            
              resetMarks();
       
      }
      function initialize() {
          // tabbss();
           loadMap(positionLat, positionLong);
      
         
      }
      var positionLat;
      var positionLong;
      function onSuccess(position) {
          var data = '';
          data = 'You are within ' + position.coords.altitude + 'meters of(' + position.coords.latitude + ';' + position.coords.longitude + ')';
          //             data += 'latitude: ' + position.coords.latitude + '<br/>';
          //             data += 'longitude: ' + position.coords.longitude + '<br/>';
          //             data += 'altitude: ' + position.coords.altitude + '<br/>';
          //             data += 'accuracy: ' + position.coords.accuracy + '<br/>';
          //             data += 'altitudeAccuracy: ' + position.coords.altitudeAccuracy + '<br/>';
          //             data += 'heading: ' + position.coords.heading + '<br/>';
          //             data += 'speed: ' + position.coords.speed + '<br/>';

          // document.getElementById('data').innerHTML = position.address.Country;
          var inputFrom = document.getElementById('fromText');
          var inputTo = document.getElementById('toText');
          var autocomplete = new google.maps.places.Autocomplete(inputFrom);
          document.getElementById('fromText').innerHTML.value = autocomplete;
          var autocomplete2 = new google.maps.places.Autocomplete(inputTo);
          document.getElementById('toText').innerHTML.value = autocomplete2;
          positionLat = position.coords.latitude;
          positionLong = position.coords.longitude;
        
      }
      function onError(err) {
          var message;

          switch (err.code) {
              case 0:
                  message = 'Unknown error: ' + err.message;
                  break;
              case 1:
                  message = 'You denied permission to retrieve a position.';
                  break;
              case 2:
                  message = 'The browser was unable to determine a position: ' + error.message;
                  break;
              case 3:
                  message = 'The browser timed out before retrieving the position.';
                  break;
          }

          //           document.getElementById('status').innerHTML = message;
      }
      var addr;
      var latlng;
      function loadMap(lat, lng) {

           latlng = new google.maps.LatLng(lat, lng);
          //    addr = new google.maps.address(lat, lng);
          var settings = {
              zoom: 14,
              center: latlng,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };

          map = new google.maps.Map(document.getElementById('map_canvas'), settings);
          if (secondBoolindex == 1) {
            
              google.maps.event.addListener(map, 'click', function (event) {
                  placeMarker(event.latLng);
              });
          }
        if (thirdBoolindex==1) {
    getTaxi();
}
        
          var marker = new google.maps.Marker({
              position: latlng,
              map: map,
              animation: google.maps.Animation.BOUNCE,
              title: 'Your Position!'
          });
        
          //         document.getElementById('status').innerHTML = 'Position Found!';
      }
      function resetMarks() {
        //  document.getElementById('temp_canvas').innerHTML = ' ';
          counter = 0;
          document.getElementById('price').innerHTML = ' ';
          document.getElementById('duration').innerHTML = ' ';
          document.getElementById('distance').innerHTML = ' ';
          document.getElementById('rangeDiv').innerHTML = ' ';
          durationText = ' ';
          priceText = ' ';
          distanceText = ' ';
          initialize();

      }

      function getAddress() {

          document.getElementById('map_canvas').style.display = '';
          
         // document.getElementById('harita').innerHTML = '<div id="button_div"></div>';
          var start = document.getElementById("fromText").value;

          var end = document.getElementById("toText").value;
          latlng = new google.maps.LatLng(positionLat, positionLong);
          directionsDisplay = new google.maps.DirectionsRenderer();
          var chicago = new google.maps.LatLng(41.850033, -87.6500523);
          var myOptions = {
              zoom: 7,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              center: latlng
          }
          map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
          directionsDisplay.setMap(map);

        
          var request = {
              origin: start,
              destination: end,
              travelMode: google.maps.TravelMode.DRIVING
          };
          directionsService.route(request, function (response, status) {
              if (status == google.maps.DirectionsStatus.OK) {
                  directionsDisplay.setDirections(response);
                  var leg = response.routes[0].legs[0];

                  makeMarker(leg.start_location, image, "title");
                  makeMarker(leg.end_location, image, 'title');
                
                  var kilometer = response.routes[0].legs[0].distance.value / 1000;
                  time = response.routes[0].legs[0].duration.value / 60;

                  // Display the distance:
                  var myRoute = response.routes[0].legs[0];
                  distanceText = '<strong class="center" data-localize="distance">Distance: </strong>' + kilometer + '<span data-localize="kilometer"> Kilometre </span>';
                  durationText = ' ';
                  //document.getElementById('distance').innerHTML = 
                  document.getElementById('rangeDiv').innerHTML = '<strong data-localize="trafficdensity">Trafik Yoğunluğu</strong><br/> <input class="1"type="range" name="grapherator1" min="1" max="100" value="1" onchange="meterCalculate1(this.value)"/>';

                  time = Math.round(time * 10) / 10;

                  calculatePrice(kilometer);
                  writeElements();

                  $("[data-localize]").localize("application")
              }
          });
    
      }
      var loc1;
      var loc2;
      var counter = 0;
      function placeMarker(location) {
               var image = 'img/taxi-icon.png';
          if (counter == 0) {
              counter = 1;
              var marker = new google.maps.Marker({
                  position: location,
                  icon: image,
                  map: map

              });
              loc1 = location;
         
          }
          else if (counter == 1) {
              counter = 2
              var marker = new google.maps.Marker({
                  position: location,
                  icon: image,
                  map: map
              });
              loc2 = location;
          
          }


      }
      var image = 'img/taxi-icon.png';
      function deneme() {
          if (counter != 2) {
              return false;
          }
          var myOptions = {
              zoom: 7,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          }

          map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
          //  directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions)

          directionsDisplay.setMap(map);
          
          var request = {
              origin: loc1,
              destination: loc2,
             
              travelMode: google.maps.DirectionsTravelMode.DRIVING
          };

          directionsService.route(request, function (response, status) {
              if (status == google.maps.DirectionsStatus.OK) {
                  directionsDisplay.setDirections(response);
                  // display.setDirections(response);
                  var leg = response.routes[0].legs[0];

                  makeMarker(leg.start_location, image, "title");
                  makeMarker(leg.end_location, image, 'title');
                  marker = new google.maps.Marker({
                      position: latlng,
                      map: map,
                      animation: google.maps.Animation.BOUNCE,
                      title: 'Your Position!'
                  });
                  var kilometer = response.routes[0].legs[0].distance.value / 1000;
                  time = response.routes[0].legs[0].duration.value / 60;

                  // Display the distance:
                  var myRoute = response.routes[0].legs[0];
                  distanceText = '<strong data-localize="distance">Distance: </strong>' + kilometer + ' Kilometre ';
                  durationText = '<strong>Adres 1: </strong>' + myRoute.start_address + '    ' + '<br/><strong>Adres 2: </strong>' + myRoute.end_address + '<br/>';
                  //document.getElementById('distance').innerHTML = 
                  document.getElementById('rangeDiv').innerHTML = '<strong>Trafik Durumu</strong><br/> <input class="1"type="range" name="grapherator1" min="1" max="100" value="1" onchange="meterCalculate1(this.value)"/>';
                 
                  time = Math.round(time * 10) / 10;



                  calculatePrice(kilometer);
                  writeElements();
                  // Display the duration:
                  //document.getElementById('duration').innerHTML = 
                  directionsDisplay.setDirections(response);

                  $("[data-localize]").localize("application");

              }
          });
      }
      var time;
      var distanceText;
      var durationText;
      var priceText;
      function makeMarker(position, icon, title) {
          new google.maps.Marker({
              position: position,
              map: map,
              icon: icon,
              title: title
          });
      }
      var result;
      function calculatePrice(km) {
          var start = 2.7;
          km = km * 1.73;
          result = start + km;
        result=  Math.round(result * 10) / 10;

      }

	function writeElements() {
          
		priceText = '<strong data-localize="fare">Fare: </strong>' + result + ' TL<br /><strong data-localize="duration">Duration:</strong>' + time + '<span data-localize="minute"> Dakika </span><br/>';
        
		document.getElementById('distance').innerHTML = distanceText;
		document.getElementById('price').innerHTML = priceText;
		document.getElementById('duration').innerHTML = durationText;

		$("[data-localize]").localize("application");
       }

       window.addEventListener('resize', resizeGame, false);
       window.addEventListener('orientationchange', resizeGame, false);
       function resizeGame() {
           var gameArea = document.getElementById('wrapper');
           var widthToHeight = 4 / 3;
           var newWidth = window.innerWidth;
           var newHeight = window.innerHeight;
           var newWidthToHeight = newWidth / newHeight;         
       }
       function meterCalculate1(number) {
           var resultTemp = result;
           var timeTemp = time;
          var pay = number - 1;
           var payda = 99;
           var son = (pay / payda) + 1;
           result = son * result;
           result = Math.round(result * 10) / 10;
           time = time * son;
           time = Math.round(time * 10) / 10;
           writeElements();
           result = resultTemp;
           time = timeTemp;
       }


             
        